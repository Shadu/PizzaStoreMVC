using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using PizzaStoreMVC.Models;

namespace PizzaStoreMVC.Authorization
{
    public interface ITokenCreation
    {
        string CreateToken(User user, string secretKey = "");
    }

    public class TokenCreation : ITokenCreation
    {
        private readonly IConfiguration _configuration;

        public TokenCreation(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public TokenCreation()
        {
        }

        public string CreateToken(User user, string secretKey = "")
        {

            if (_configuration != null)
            {
                secretKey = _configuration.GetValue<String>("AppSettings:SecretKey");
            }

            // SymmetricSecurityKey can't work with a string, UTF8 is chosen here to convert to.
            var key = Encoding.UTF8.GetBytes(secretKey);
            var skey = new SymmetricSecurityKey(key);

            // Creates a signature to supply with the key.
            var SignedCredential = new SigningCredentials(skey, SecurityAlgorithms.HmacSha256Signature);

            // Generates a unique id for the token.
            var jti = Guid.NewGuid().ToString();

            // Defining the claims to be used in the token.
            var uClaims = new ClaimsIdentity(new[]
            {
                new Claim(JwtRegisteredClaimNames.UniqueName,"shadu"),
                new Claim(JwtRegisteredClaimNames.Sub,"shadu"),
                new Claim(JwtRegisteredClaimNames.Jti,jti),
                new Claim("scope","greetings_api"),
                new Claim("role", user.Role),
                new Claim(JwtRegisteredClaimNames.Aud,"http://localhost:5099"),
                new Claim(JwtRegisteredClaimNames.Aud,"https://localhost:7062"),
            });

            // Setting an expiry date for the token.
            var expires = DateTime.UtcNow.AddDays(1);

            // Setting the "body" of the token.
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = uClaims,
                Expires = expires,
                Issuer = "dotnet-user-jwts",
                SigningCredentials = SignedCredential,
            };

            // Creating the token.
            var tokenHandler = new JwtSecurityTokenHandler();
            var tokenJwt = tokenHandler.CreateToken(tokenDescriptor);
            var token = tokenHandler.WriteToken(tokenJwt);

            return token;
        }
    }

}
