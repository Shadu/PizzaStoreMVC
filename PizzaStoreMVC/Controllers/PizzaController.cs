using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using PizzaStoreMVC.Models;
using Logic.Services;
using PizzaStoreMVC.Repositories;

namespace PizzaStoreMVC.Controllers
{
    [Authorize(Policy = "admin_greetings")]
    [Route("api/[controller]")]
    [ApiController]
    public class PizzaController : ControllerBase
    {
        private readonly IPizzaService _pizzaService;

        // private IPizzaStoreContext _context = new PizzaContext();

        // public PizzaController() { }

        public PizzaController(IPizzaService pizzaService)
        {
            _pizzaService = pizzaService;
        }

        // public PizzaController(IPizzaStoreContext context)
        // {
        //     _context = context;
        // }

        // GET: api/Pizza
        [Authorize(Roles = "user,admin")]
        [HttpGet]
        public async Task<IActionResult> GetPizza()
        {
            try
            {
                var pizzas = await _pizzaService.GetAll();

                return Ok(pizzas);
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        // GET: api/Pizza/5
        [Authorize(Roles = "user,admin")]
        [HttpGet("{id}")]
        public async Task<IActionResult> GetPizza(int id)
        {
            try
            {
                var pizza = await _pizzaService.GetById(id);

                return Ok(pizza);
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        // PUT: api/Pizza/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [Authorize(Roles = "admin")]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPizza(int id, Pizza entity)
        {
            try
            {
                var pizza = await _pizzaService.Update(id, entity);
                return Ok(pizza);
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        // POST: api/Pizza
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [Authorize(Roles = "admin")]
        [HttpPost]
        public async Task<IActionResult> PostPizza(Pizza entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }
            try
            {
                var pizza = await _pizzaService.Create(entity);

                return Ok(pizza);
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        // DELETE: api/Pizza/5
        [Authorize(Roles = "admin")]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePizza(int id)
        {
            try
            {
                await _pizzaService.Delete(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        // private bool PizzaExists(int id)
        // {
        //     return (_pizzaService.Pizzas?.Any(e => e.Id == id)).GetValueOrDefault();
        // }
    }
}
