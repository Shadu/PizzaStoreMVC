using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

using PizzaStoreMVC.Authorization;

namespace PizzaStoreMVC.Controllers
{
    [AllowAnonymous]
    [Route("api/[controller]")]
    [ApiController]
    public class TokenCreationController : ControllerBase
    {
        private readonly ITokenCreation _tokenCreation;

        public TokenCreationController(ITokenCreation tokenCreation)
        {
            _tokenCreation = tokenCreation;
        }

        [HttpPost]
        public IActionResult CreateAccessToken(User user)
        {
            // _logger.LogInformation("TokenCreationController endpoint accessed.");

            if (user.Role == null)
            {
                throw new ArgumentNullException();
            }

            var token = _tokenCreation.CreateToken(user);

            return Ok(new { Token = token });
        }

    }
}
