using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PizzaStoreMVC.Models;
using Logic.Exceptions;

namespace PizzaStoreMVC.Repositories
{
    public interface IPizzaRepository
    {
        Task<Pizza> Create(Pizza entity);
        Task<Pizza> GetById(int id);
        Task<Pizza> Update(int id, Pizza entity);
        Task<List<Pizza>> GetAll();
        Task Delete(int id);

    }

    public class PizzaRepository : IPizzaRepository
    {
        private readonly IPizzaStoreContext _context;


        public PizzaRepository(IPizzaStoreContext context)
        {
            _context = context;
        }

        public async Task<List<Pizza>> GetAll()
        {

            var all = await _context.Pizzas.ToListAsync();

            return all;
        }

        public async Task<Pizza> GetById(int id)
        {
            if (_context.Pizzas == null)
            {
                throw new NotFoundException($"Pizza with id {id} not found");
            }
            var pizza = await _context.Pizzas.FindAsync(id);

            if (pizza == null)
            {
                throw new NotFoundException($"Pizza with id {id} not found");
            }

            return pizza;
        }

        public async Task<Pizza> Update(int id, Pizza pizza)
        {
            if (id != pizza.Id)
            {
                throw new NotFoundException($"Pizza with id {id} not found");
            }

            _context.Pizzas.Entry(pizza).State = EntityState.Modified;

            try
            {
                _context.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PizzaExists(id))
                {
                    throw new NotFoundException($"Pizza with id {id} not found");
                }
                else
                {
                    throw;
                }
            }

            var updatedPizza = await GetById(id);
            Console.WriteLine($"Pizza is now {updatedPizza.Name}");
            return updatedPizza;
        }

        public async Task<Pizza> Create(Pizza pizza)
        {
            if (_context.Pizzas == null)
            {
                throw new NotFoundException("Entity set 'PizzaContext.Pizza'  is null.");
            }
            _context.Pizzas.Add(pizza);
            _context.SaveChanges();
            var newPizza = await GetById(pizza.Id);
            return newPizza;
        }

        public async Task Delete(int id)
        {
            if (_context.Pizzas == null)
            {
                throw new NotFoundException("Entity set 'PizzaContext.Pizza'  is null.");
            }
            var pizza = await _context.Pizzas.FindAsync(id);
            if (pizza == null)
            {
                throw new NotFoundException("Pizza with id {id} was not found.");
            }

            _context.Pizzas.Remove(pizza);
            _context.SaveChanges();
        }

        private bool PizzaExists(int id)
        {
            return (_context.Pizzas?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
