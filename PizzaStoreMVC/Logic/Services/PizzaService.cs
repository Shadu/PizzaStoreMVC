using PizzaStoreMVC.Models;
using PizzaStoreMVC.Repositories;
using Logic.Exceptions;

namespace Logic.Services
{
    public interface IPizzaService
    {
        Task<Pizza> Create(Pizza entity);
        Task<Pizza> GetById(int id);
        Task<Pizza> Update(int id, Pizza entity);
        Task<List<Pizza>> GetAll();
        Task Delete(int id);

    }

    public class PizzaService : IPizzaService
    {

        private readonly IPizzaRepository _pizzaRepository;

        public PizzaService(IPizzaRepository pizzaRepository)
        {
            _pizzaRepository = pizzaRepository;
        }


        public async Task<Pizza> Create(Pizza entity)
        {
            var pizza = await _pizzaRepository.Create(entity);

            return pizza;
        }

        public async Task<Pizza> GetById(int id)
        {
            var pizza = await _pizzaRepository.GetById(id);
            if (pizza == null)
            {
                throw new NotFoundException("Pizza not found");
            }
            return pizza;
        }

        public async Task<Pizza> Update(int id, Pizza entity)
        {
            var pizza = await _pizzaRepository.Update(id, entity);
            if (pizza == null)
            {
                throw new NotFoundException("Pizza not found");
            }
            return pizza;
        }
        public async Task<List<Pizza>> GetAll()
        {

            var allPizzas = await _pizzaRepository.GetAll();
            if (allPizzas == null)
            {
                throw new NotFoundException("Pizza not found");
            }
            return allPizzas;

        }
        public async Task Delete(int id)
        {
            await _pizzaRepository.Delete(id);
        }
    }
}
