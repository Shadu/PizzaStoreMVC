
using Microsoft.EntityFrameworkCore;

namespace PizzaStoreMVC.Models
{

    public interface IPizzaStoreContext : IDisposable
    {
        DbSet<Pizza> Pizzas { get; }
        int SaveChanges();
        void MarkAsModified(Pizza item);
    }

    public class PizzaContext : DbContext, IPizzaStoreContext
    {
        public PizzaContext(DbContextOptions<PizzaContext> options)
            : base(options)
        {
        }

        public PizzaContext()
            : base()
        {
        }
        public DbSet<Pizza> Pizzas { get; set; } = null!;

        public void MarkAsModified(Pizza item)
        {
            Entry(item).State = EntityState.Modified;
        }

    }

    // public class PizzaContext : DbContext
    // {
    //     public virtual DbSet<Pizza> Pizzas { get; set; }
    // }
}
