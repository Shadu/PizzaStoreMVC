// using Microsoft.AspNetCore.Authentication;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using System.Text;
using Logic.Services;
using PizzaStoreMVC.Repositories;
using PizzaStoreMVC.Models;
using PizzaStoreMVC.Authorization;

// using Microsoft.Extensions.DependencyInjection;

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddDbContext<PizzaContext>(options =>
    options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection") ?? throw new InvalidOperationException("Connection string 'PizzaContext' not found.")));

// var connectionString = builder.Configuration.GetConnectionString("DefaultConnection");

// Add services to the container.

builder.Services.AddControllers();
// builder.Services.AddDbContext<PizzaDb>(options => options.UseSqlServer(connectionString)) p;
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
// Adding swagger and telling it to use authorization.
builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v1", new OpenApiInfo { Title = "PizzaStore API", Description = "Making the Pizzas you love", Version = "v1" });
    c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
    {
        Scheme = "Bearer",
        BearerFormat = "JWT",
        In = ParameterLocation.Header,
        Name = "Authorization",
        Type = SecuritySchemeType.Http
    });
    c.AddSecurityRequirement(new OpenApiSecurityRequirement
    {
                        {
                            new OpenApiSecurityScheme
                            {
                                Reference = new OpenApiReference
                                {
                                    Id = "Bearer",
                                    Type = ReferenceType.SecurityScheme
                                }
                            },
                            new string[] {}
                        }
    });
});
builder.Services.AddAuthentication().AddJwtBearer(options =>
{
    options.TokenValidationParameters = new TokenValidationParameters
    {
        ValidateIssuerSigningKey = true,
        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(builder.Configuration.GetValue<string>("AppSettings:SecretKey"))),
        ValidAudiences = new[] { "http://localhost:5099", "https://localhost:7062" },
        ValidIssuer = "dotnet-user-jwts"

    };
});

// Adding the policy to check for.
builder.Services.AddAuthorizationBuilder().AddPolicy("admin_greetings", policy => policy
.RequireRole("admin", "user")
.RequireClaim("scope", "greetings_api"));

builder.Services.AddScoped<IPizzaStoreContext, PizzaContext>();
builder.Services.AddScoped<IPizzaService, PizzaService>();
builder.Services.AddScoped<IPizzaRepository, PizzaRepository>();
builder.Services.AddScoped<ITokenCreation, TokenCreation>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI(c =>
    {
        c.SwaggerEndpoint("/swagger/v1/swagger.json", "PizzaStore API V1");
    });
}

if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Error");
    app.UseHsts();
}

app.Use(async (context, next) =>
{
    Console.WriteLine($"Request received for: {context.Request.Path}");
    // Read the request body
    using (var reader = new StreamReader(context.Request.Body))
    {
        string requestBody = await reader.ReadToEndAsync();
        Console.WriteLine($"Body received: {requestBody}");
        // Create a new stream with the same data
        var requestBodyStream = new MemoryStream();
        var bytes = Encoding.UTF8.GetBytes(requestBody);
        requestBodyStream.Write(bytes, 0, bytes.Length);
        requestBodyStream.Seek(0, SeekOrigin.Begin);
        // Replace the original Request.Body with the new stream
        context.Request.Body = requestBodyStream;
    }
    await next.Invoke();
});


app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();

public partial class Program { }
