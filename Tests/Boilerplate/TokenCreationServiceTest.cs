using System.IdentityModel.Tokens.Jwt;

using PizzaStoreMVC.Authorization;

namespace PizzaStoreMVC.Tests
{
    public class TokenCreationTests
    {
        private readonly TokenCreation _creation;
        private readonly User newUser = new User { Role = "admin" };


        public TokenCreationTests()
        {
            _creation = new TokenCreation();
        }

        [Fact]
        public void RequestToken_ReturnsOkResultAndJWT()
        {
            var result = _creation.CreateToken(newUser, "12345678901234567890123456789012345678901234567890123456789012345678901234567890");
            Assert.NotNull(result);
            var tokenHandler = new JwtSecurityTokenHandler();
            var jwtToken = tokenHandler.ReadJwtToken(result);
            var actualClaims = jwtToken.Claims.ToList();
            string scope = "";
            string role = "";
            foreach (var claim in actualClaims)
            {
                if (claim.Type == "scope")
                {
                    scope = claim.Value;
                }
                else if (claim.Type == "role")
                {
                    role = claim.Value;
                }
            }
            Assert.Equal("greetings_api", scope);
            Assert.Equal(newUser.Role, role);
        }

    }

}
