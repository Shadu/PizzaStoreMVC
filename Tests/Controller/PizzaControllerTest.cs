using Microsoft.AspNetCore.Mvc;
using Moq;
using PizzaStoreMVC.Controllers;
using PizzaStoreMVC.Models;
using Logic.Services;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace PizzaStoreMVC.Tests
{
    public class PizzaControllerTests
    {
        private readonly PizzaController _controller;
        private readonly Mock<IPizzaService> _mockService;
        private List<Pizza> pizzas;
        private Pizza pizza;
        private Pizza newPizza;

        public PizzaControllerTests()
        {
            _mockService = new Mock<IPizzaService>();
            _controller = new PizzaController(_mockService.Object);
            pizzas = TestPizzas();
            pizza = new Pizza { Id = 2, Name = "Salami" };
            newPizza = new Pizza { Name = "Salami", Description = "Salami Pizza" };
            SetupMockedService();
        }

        [Fact]
        public async Task GetPizza_ReturnsOkResultAndPizzaList()
        {
            var result = await _controller.GetPizza();
            var okResult = Assert.IsType<OkObjectResult>(result);
            var resultContent = okResult.Value as List<Pizza>;
            Assert.NotNull(resultContent);
            Assert.True(JToken.DeepEquals(JArray.FromObject(TestPizzas()), JArray.FromObject(resultContent)));
        }

        [Fact]
        public async Task GetPizzaByID_ReturnsOkResultAndPizza()
        {
            var jsonArray = JArray.FromObject(TestPizzas());
            var expectedPizza = jsonArray[1];
            var result = await _controller.GetPizza(2);
            var okResult = Assert.IsType<OkObjectResult>(result);
            var resultContent = okResult.Value as Pizza;
            Assert.NotNull(resultContent);
            string json = JsonConvert.SerializeObject(resultContent);
            JToken jToken = JToken.Parse(json);
            Assert.True(JToken.DeepEquals(expectedPizza, jToken));
        }

        [Fact]
        public async Task UpdatePizzaByID_ReturnsOkResultAndUpdatedPizza()
        {
            var result = await _controller.PutPizza(2, pizza);
            var okResult = Assert.IsType<OkObjectResult>(result);
            var resultContent = okResult.Value as Pizza;
            Assert.NotNull(resultContent);
            Assert.Equal(pizza, resultContent);
        }

        [Fact]
        public async Task CreateNewPizza_ReturnsOkResultAndNewPizza()
        {
            var result = await _controller.PostPizza(newPizza);
            var okResult = Assert.IsType<OkObjectResult>(result);
            var resultContent = okResult.Value as Pizza;
            Assert.NotNull(resultContent);
            Assert.Equal(newPizza, resultContent);
        }

        [Fact]
        public async Task DeletePizza_ReturnsOkResult()
        {
            var result = await _controller.DeletePizza(2);
            Assert.IsType<OkResult>(result);
        }

        private List<Pizza> TestPizzas()
        {
            return new List<Pizza>{

            new Pizza { Id = 1, Name = "Margherita" },
            new Pizza { Id = 2, Name = "Pepperoni" },
            new Pizza { Id = 3, Name = "Hawaiian" }
                };
        }

        private void SetupMockedService()
        {
            _mockService.Setup(service => service.GetAll()).ReturnsAsync(TestPizzas());
            _mockService.Setup(service => service.GetById(2)).ReturnsAsync(TestPizzas()[1]);
            _mockService.Setup(service => service.Update(2, pizza)).ReturnsAsync((int id, Pizza pizza) =>
            {
                pizzas[id] = pizza;
                return pizzas[id];
            });
            _mockService.Setup(service => service.Create(newPizza)).ReturnsAsync((Pizza pizza) =>
            {
                pizzas.Add(pizza);
                return pizzas.Last();
            });
            _mockService.Setup(service => service.Delete(2));
        }
    }

}
