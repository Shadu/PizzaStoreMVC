using Microsoft.AspNetCore.Mvc;
using Moq;
using PizzaStoreMVC.Controllers;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

using PizzaStoreMVC.Authorization;

namespace PizzaStoreMVC.Tests
{
    public class TokenControllerTests
    {
        private readonly TokenCreationController _controller;
        private readonly Mock<ITokenCreation> _mockService;
        private readonly User newUser = new User { Role = "admin" };


        public TokenControllerTests()
        {
            _mockService = new Mock<ITokenCreation>();
            _controller = new TokenCreationController(_mockService.Object);
            SetupMockedService();
        }

        [Fact]
        public void RequestToken_ReturnsOkResultAndString()
        {
            var expectedResponse = new { Token = newUser.Role };
            string expectedJson = JsonConvert.SerializeObject(expectedResponse);
            JToken expectedjToken = JToken.Parse(expectedJson);
            _mockService.Setup(service => service.CreateToken(newUser, "")).Returns(() => { Console.WriteLine("Testing"); return newUser.Role; });
            var result = _controller.CreateAccessToken(newUser);
            var okResult = Assert.IsType<OkObjectResult>(result);
            var resultContent = okResult.Value;
            Assert.NotNull(resultContent);
            string json = JsonConvert.SerializeObject(resultContent);
            JToken jToken = JToken.Parse(json);
            Assert.True(JToken.DeepEquals(expectedjToken, jToken));
        }

        private void SetupMockedService()
        {
            _mockService.Setup(service => service.CreateToken(newUser, "")).Returns(() => { Console.WriteLine("Testing"); return newUser.Role; });
        }

    }

}
