using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using PizzaStoreMVC.Models;
using PizzaStoreMVC.Repositories;
using FluentAssertions;

namespace PizzaStoreMVC.Tests
{
    public class PizzaRepositoryTests
    {
        private readonly SqliteConnection _connection;

        private readonly DbContextOptions<PizzaContext> _contextOptions;

        private readonly List<Pizza> _pizzaList;

        public PizzaRepositoryTests()
        {
            _connection = new SqliteConnection("Filename=:memory:");
            _connection.Open();

            _contextOptions = new DbContextOptionsBuilder<PizzaContext>()
                .UseSqlite(_connection)
                .Options;

            using var context = new PizzaContext(_contextOptions);

            _pizzaList = pizzaList();

            if (context.Database.EnsureCreated())
            {
                using var viewCommand = context.Database.GetDbConnection().CreateCommand();
                viewCommand.CommandText = @"
CREATE VIEW ALLResources AS
SELECT Name
FROM Pizza;";
                viewCommand.ExecuteNonQuery();
            }
            context.AddRange(_pizzaList);
            context.SaveChanges();

        }

        PizzaContext CreateContext() => new PizzaContext(_contextOptions);

        [Fact]
        public async Task GetAllPizza_ReturnsPizzaList()
        {
            using var context = CreateContext();
            var repository = new PizzaRepository(context);

            var pizzas = await repository.GetAll();
            foreach (var pizza in pizzas)
            {
                Console.WriteLine($"Pizza name: {pizza.Name} id: {pizza.Id}");
            }
            pizzas.Should().BeEquivalentTo(_pizzaList);
        }

        [Fact]
        public async Task GetById_ReturnsPizza()
        {
            using var context = CreateContext();
            var repository = new PizzaRepository(context);
            var pizza = await repository.GetById(2);
            pizza.Should().BeEquivalentTo(_pizzaList[1]);
        }

        [Fact]
        public async Task UpdatePizza_ReturnsPizza()
        {
            Pizza newPizza = new Pizza { Id = 2, Name = "Salami" };
            using var context = CreateContext();
            var repository = new PizzaRepository(context);
            var pizza = await repository.Update(2, newPizza);
            pizza.Should().BeEquivalentTo(newPizza);
        }

        [Fact]
        public async Task AddPizza_ReturnsPizza()
        {
            Pizza newPizza = new Pizza { Name = "Salami" };
            using var context = CreateContext();
            var repository = new PizzaRepository(context);
            var pizza = await repository.Create(newPizza);
            pizza.Should().BeEquivalentTo(newPizza);
        }

        [Fact]
        public async Task DeletePizza_ReturnsNothing()
        {
            using var context = CreateContext();
            var repository = new PizzaRepository(context);
            var result = await context.Pizzas.FindAsync(2);
            Assert.NotNull(result);
            await repository.Delete(2);
            result = await context.Pizzas.FindAsync(2);
            Assert.Null(result);
        }

        private List<Pizza> pizzaList()
        {
            return new List<Pizza> {

            new Pizza { Id = 1, Name = "Margherita" },
            new Pizza { Id = 2, Name = "Pepperoni" },
            new Pizza { Id = 3, Name = "Hawaiian" }
           };
        }

    }
}
