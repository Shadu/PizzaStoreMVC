using PizzaStoreMVC.Models;

namespace PizzaStoreMVC.Tests.Helpers
{
    public static class Utilities
    {

        public static void InitializeDbForTests(PizzaContext db)
        {
            db.Pizzas.AddRange(GetSeedingPizzas());
            db.SaveChanges();
        }

        public static void ReinitializeDbForTests(PizzaContext db)
        {
            db.Pizzas.RemoveRange(db.Pizzas);
            db.Pizzas.AddRange(GetSeedingPizzas());
            db.SaveChanges();
        }

        public static List<Pizza> GetSeedingPizzas()
        {
            return new List<Pizza>()
    {
            new Pizza { Id = 1, Name = "Margherita" },
            new Pizza { Id = 2, Name = "Pepperoni" },
            new Pizza { Id = 3, Name = "Hawaiian" }
    };
        }
    }
}
