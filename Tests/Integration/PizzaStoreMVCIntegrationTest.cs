using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IdentityModel.Tokens.Jwt;
using System.Net.Http.Json;
using System.Text;

using PizzaStoreMVC.Models;
using PizzaStoreMVC.Tests.Helpers;

namespace PizzaStoreMVC.Tests.IntegrationTests
{
    public class IntegrationTest : IClassFixture<CustomWebApplicationFactory<Program>>, IDisposable
    {
        private HttpClient _httpClient;

        private readonly CustomWebApplicationFactory<Program> _factory;

        public IntegrationTest(CustomWebApplicationFactory<Program> factory)
        {
            _factory = factory;
            _httpClient = factory.CreateClient(new WebApplicationFactoryClientOptions
            {
                AllowAutoRedirect = false
            });
            using (var scope = _factory.Services.CreateScope())
            {
                var scopedServices = scope.ServiceProvider;
                var db = scopedServices.GetRequiredService<PizzaContext>();
                db.Database.EnsureCreated();
                if (!db.Pizzas.Any())
                {
                    Utilities.InitializeDbForTests(db);
                }
            }
        }

        [Fact]
        public async Task PostRole_ReturnsValidToken()
        {
            var jsonData = new
            {
                role = "admin",
            };

            var jsonString = JsonConvert.SerializeObject(jsonData);

            var content = new StringContent(jsonString, Encoding.UTF8, "application/json");
            var response = await _httpClient.PostAsync("api/TokenCreation", content);
            var stringResult = await response.Content.ReadAsStringAsync();
            JObject jsonObject = JObject.Parse(stringResult);
            var token = jsonObject["token"].ToString();
            Console.WriteLine($"Token Value {token}");
            var tokenHandler = new JwtSecurityTokenHandler();
            var jwtToken = tokenHandler.ReadJwtToken(token);
            var actualClaims = jwtToken.Claims.ToList();
            string scope = "";
            string role = "";
            foreach (var claim in actualClaims)
            {
                if (claim.Type == "scope")
                {
                    scope = claim.Value;
                }
                else if (claim.Type == "role")
                {
                    role = claim.Value;
                }
            }
            Assert.Equal("greetings_api", scope);
            Assert.Equal("admin", role);
        }

        [Fact]
        public async Task GetAllPizzas_ReturnsAllPizzas()
        {
            string token = await requestToken();
            _httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);
            var response = await _httpClient.GetAsync("api/pizza");
            Assert.True(response.IsSuccessStatusCode);
            var result = await response.Content.ReadFromJsonAsync<List<Pizza>>();
            Assert.NotNull(result);
            Assert.IsType<Pizza>(result[0]);
        }

        [Fact]
        public async Task GetPizzaByID_ReturnsOkResultAndPizza()
        {
            string token = await requestToken();
            _httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);
            var response = await _httpClient.GetAsync("api/pizza/1");
            Assert.True(response.IsSuccessStatusCode);
            var result = await response.Content.ReadFromJsonAsync<Pizza>();
            Assert.NotNull(result);
            Assert.IsType<Pizza>(result);
        }

        [Fact]
        public async Task UpdatePizzaByID_ReturnsOkResultAndUpdatedPizza()
        {
            string token = await requestToken();
            _httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);
            Pizza newPizza = new Pizza { Id = 1, Name = "Cheese" };
            var jsonString = JsonConvert.SerializeObject(newPizza);
            var content = new StringContent(jsonString, Encoding.UTF8, "application/json");
            var response = await _httpClient.PutAsync("api/pizza/1", content);
            Assert.True(response.IsSuccessStatusCode);
            var result = await response.Content.ReadFromJsonAsync<Pizza>();
            Assert.NotNull(result);
            Assert.IsType<Pizza>(result);
        }

        [Fact]
        public async Task CreateNewPizza_ReturnsOkResultAndNewPizza()
        {
            string token = await requestToken();
            _httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);
            Pizza newPizza = new Pizza { Name = "Chese", Description = "Cheese Pizza" };
            var jsonString = JsonConvert.SerializeObject(newPizza);
            var content = new StringContent(jsonString, Encoding.UTF8, "application/json");
            var response = await _httpClient.PostAsync("api/pizza", content);
            Assert.True(response.IsSuccessStatusCode);
            var result = await response.Content.ReadFromJsonAsync<Pizza>();
            Assert.NotNull(result);
            Assert.IsType<Pizza>(result);
        }

        [Fact]
        public async Task DeletePizza_ReturnsOkResult()
        {
            string token = await requestToken();
            _httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);
            var response = await _httpClient.DeleteAsync("api/pizza/1");
            Assert.True(response.IsSuccessStatusCode);
        }

        public void Dispose()
        {
            using (var scope = _factory.Services.CreateScope())
            {
                var scopedServices = scope.ServiceProvider;
                var db = scopedServices.GetRequiredService<PizzaContext>();
                Utilities.ReinitializeDbForTests(db);
            }
        }

        private async Task<string> requestToken()
        {
            var jsonData = new
            {
                role = "admin",
            };

            var jsonString = JsonConvert.SerializeObject(jsonData);

            var content = new StringContent(jsonString, Encoding.UTF8, "application/json");
            var response = await _httpClient.PostAsync("api/TokenCreation", content);
            var stringResult = await response.Content.ReadAsStringAsync();
            JObject jsonObject = JObject.Parse(stringResult);
            var token = jsonObject["token"].ToString();
            return token;
        }
    }
}
