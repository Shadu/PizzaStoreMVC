using Moq;
using PizzaStoreMVC.Models;
using Logic.Services;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

using PizzaStoreMVC.Authorization;
using PizzaStoreMVC.Repositories;

namespace PizzaStoreMVC.Tests
{
    public class PizzaServiceTests
    {
        private readonly IPizzaService _service;
        private readonly Mock<IPizzaRepository> _mockService;
        private readonly User newUser = new User { Role = "admin" };
        private List<Pizza> pizzas;
        private Pizza pizza;
        private Pizza newPizza;

        public PizzaServiceTests()
        {
            _mockService = new Mock<IPizzaRepository>();
            _service = new PizzaService(_mockService.Object);
            pizzas = TestPizzas();
            pizza = new Pizza { Id = 2, Name = "Salami" };
            newPizza = new Pizza { Name = "Salami", Description = "Salami Pizza" };
            SetupMockedService();
        }

        [Fact]
        public async Task GetPizza_ReturnsPizzaList()
        {
            var result = await _service.GetAll();
            Assert.IsType<List<Pizza>>(result);
            Assert.NotNull(result);
            Assert.True(JToken.DeepEquals(JArray.FromObject(TestPizzas()), JArray.FromObject(result)));
        }

        [Fact]
        public async Task GetPizzaByID_ReturnsPizza()
        {
            var jsonArray = JArray.FromObject(TestPizzas());
            var expectedPizza = jsonArray[1];
            var result = await _service.GetById(2);
            Assert.IsType<Pizza>(result);
            Assert.NotNull(result);
            string json = JsonConvert.SerializeObject(result);
            JToken jToken = JToken.Parse(json);
            Assert.True(JToken.DeepEquals(expectedPizza, jToken));
        }

        [Fact]
        public async Task UpdatePizzaByID_ReturnsUpdatedPizza()
        {
            var result = await _service.Update(2, pizza);
            Assert.IsType<Pizza>(result);
            Assert.NotNull(result);
            Assert.Equal(pizza, result);
        }

        [Fact]
        public async Task CreateNewPizza_ReturnsNewPizza()
        {
            var result = await _service.Create(newPizza);
            Assert.IsType<Pizza>(result);
            Assert.NotNull(result);
            Assert.Equal(newPizza, result);
        }

        private List<Pizza> TestPizzas()
        {
            return new List<Pizza>{

            new Pizza { Id = 1, Name = "Margherita" },
            new Pizza { Id = 2, Name = "Pepperoni" },
            new Pizza { Id = 3, Name = "Hawaiian" }
                };
        }

        private void SetupMockedService()
        {
            _mockService.Setup(service => service.GetAll()).ReturnsAsync(TestPizzas());
            _mockService.Setup(service => service.GetById(2)).ReturnsAsync(TestPizzas()[1]);
            _mockService.Setup(service => service.Update(2, pizza)).ReturnsAsync((int id, Pizza pizza) =>
            {
                pizzas[id] = pizza;
                return pizzas[id];
            });
            _mockService.Setup(service => service.Create(newPizza)).ReturnsAsync((Pizza pizza) =>
            {
                pizzas.Add(pizza);
                return pizzas.Last();
            });
            _mockService.Setup(service => service.Delete(2));
        }

    }

}
